package main

import(
    "fmt"
)

func main() {
    var list = []int{10, -1, 3, 99, -20, 7};
    list = bubbleSort(list);

    fmt.Println(list)
}

func bubbleSort(list []int) []int {
    var sortedList = list;

    for ix := 0; ix < len(sortedList); ix++ {
        for iy := ix; iy < len(sortedList); iy++ {
            if sortedList[ix] > sortedList[iy] {
                sortedList[ix], sortedList[iy] = sortedList[iy], sortedList[ix];
            }
        }
    }

    return sortedList;
}
