package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type authResponse struct {
	Token   string
	Message string
}

func checkCredentials(username string, password string) bool {
	hcdName := "test"
	hcdPass := "test"

	return hcdName == username && hcdPass == password
}

func authHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")
		if err := r.ParseForm(); err != nil {
			http.Error(w, "Invalid request method.", http.StatusBadRequest)
			return
		}

		username := r.FormValue("username")
		password := r.FormValue("password")
		response := authResponse{"", "Invalid credentials"}

		if checkCredentials(username, password) {
			response = authResponse{"_34fdsaz872", "Authenticated"}
		}
		js, err := json.Marshal(response)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(js)
	} else {
		http.Error(w, "Invalid request method.", http.StatusMethodNotAllowed)
	}
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	if validateRequest(w, r) == false {
		http.Error(w, "Provided token is invalid.", http.StatusUnauthorized)
		return
	}
	w.Write([]byte(`{"Message":"Hello"}`))
}

func validateRequest(w http.ResponseWriter, r *http.Request) bool {
	valid := true
	authentication := r.Header.Get("Authorization")
	if len(strings.TrimSpace(authentication)) == 0 {
		valid = false
	}

	auth := strings.Split(authentication, " ")
	if len(auth) != 2 {
		valid = false
	}

	valid = (auth[0] == "basic" && auth[1] == "_34fdsaz872")

	return valid
}

func main() {
	http.HandleFunc("/authenticate", authHandler)
	http.HandleFunc("/say-hello", sayHello)
	fmt.Printf("Starting server for testing HTTP POST...\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
