package main

import (
	"fmt"
	"reflect"
)

func main() {
	const testString = "abcdababc"

	for i := 1; i <= len(testString)/2; i++ {
		searchRepeatedStrings(i, testString)
	}

}

func searchRepeatedStrings(length int, testString string) {
	var chunk = []byte(testString)
	var chunks []string
	for i := 0; i < len(testString); i++ {
		if chunkExists(chunks, string(chunk[i:length+i])) {
			continue
		} else {
			chunks = append(chunks, string(chunk[i:length+i]))
		}
		for j := (i + 1); j < len(testString); j++ {
			if length+j <= len(testString) && reflect.DeepEqual(chunk[i:length+i], chunk[j:length+j]) {
				fmt.Println("Found duplicated chunks: ", string(chunk[i:length+i]), " == ", string(chunk[j:length+j]))
			}
		}
	}
}

func chunkExists(chunks []string, val string) bool {
	for _, item := range chunks {
		if item == val {
			return true
		}
	}
	return false
}
