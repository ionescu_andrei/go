package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "path/filepath"
)

func main() {
    readDir("./", "")
}

func readDir(dir string, level string) {
    files, err := ioutil.ReadDir(dir)
    if err != nil {
        log.Fatal(err)
    }

    for _, f := range files {
        fmt.Println(level + f.Name())
        if (f.IsDir()) {
            path := filepath.FromSlash(dir + "/" + f.Name())
            readDir(path, level + " - ")
        }
    }
}
