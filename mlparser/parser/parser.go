package parser

import (
    // "fmt"
)

const (
    openedTag string = "<"
    openedSecondTag string = "</"
    closedTag string = ">"
)

type XMLElement struct {
    startTag string
    endTag string
    contents []*XMLElement
}

func Parse(content string) *XMLElement {
    var rootElement = &XMLElement{"", "", nil}
    var currentElement = &rootElement

    for y := 0; y < len(content); y++ {
        currentChar := string(content[y])
        if (currentChar == openedTag && string(content[y + 1]) == "/") {
            FillElementFrom(&(*currentElement).endTag, &content, &y)
        } else if(currentChar == openedTag) {
            FillElementFrom(&(*currentElement).startTag, &content, &y)
        } else {
            //(*currentElement).contents = append((*currentElement).contents, &XMLElement{"", []*XMLElement{}})
        }
    }

    return rootElement
}

func FillElementFrom(elementProp *string, content *string, index *int) {
    var char string;
    for {
        char = string((*content)[*index])

        *elementProp += char
        if char == closedTag || *index >= len(*content) {
            break;
        }
        *index++
    }
}

