package main

import (
    "io/ioutil"
    "log"
    ml "./parser"
    "fmt"
)

func main() {
    content, err := ioutil.ReadFile("test.xml")
    if err != nil {
        log.Fatal(err)
    }

    element := ml.Parse(string(content))

    fmt.Print((*element))
}


